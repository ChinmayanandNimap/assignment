import React, { useState } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import './sideBar.css';
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";


const drawerWidth = "16%";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    zIndex: 0
  },
  nested: {
    paddingLeft: "50px",
   color:'white'
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },

  
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: {minHeight: 104},
  drawerPaper: {
    width: drawerWidth,
    backgroundColor:'black',

  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function SideBar(props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };  
const handleClick=()=>{
  setOpen(!open)
}
  const drawer = (
    <div className="h-100 sidebar-bg nav-link hover px-0">
      <div className={classes.toolbar}/>
      <List>
      <ListItem className="px-10" button >
        <Link  style={{textDecorationLine:'none',color:'white'}} to="/">
           <ListItemText primary="Home"/>
           </Link>
        </ListItem>
        <ListItem className="px-10" button onClick={handleClick}>
           <ListItemText style={{textDecorationLine:'none',color:'white'}} primary="Data"/>
           {open ? (
                  <ExpandLess style={{ color: "white" }} />
                ) : (
                  <ExpandMore style={{ color: "white" }} />
                )}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <ListItem button className={classes.nested}>
                    <ListItemIcon>
                    </ListItemIcon>
                    <Link style={{textDecorationLine:'none'}} to="/data">
                      {" "}
                      <ListItemText
                      style={{color:'white'}}
                       
                        primary="Bar Data"
                      />{" "}
                    </Link>
                  </ListItem>
                </List>
                <List component="div" disablePadding>
                  <ListItem button className={classes.nested}>
                    <ListItemIcon>
                    </ListItemIcon>
                    <Link style={{textDecorationLine:'none'}} to="/admin/user/userPermission">
                      {" "}
                      <ListItemText
                       style={{color:'white'}}
                        primary="Graph Data"
                      />{" "}
                    </Link>
                  </ListItem>
                </List>
              </Collapse>

              <ListItem className="px-10" button>
           <ListItemText style={{textDecorationLine:'none',color:'white'}} primary="About-Us"/>
        </ListItem>
        <ListItem className="px-10" button>
           <ListItemText style={{textDecorationLine:'none',color:'white'}} primary="COntact-Us"/>
        </ListItem>
        <ListItem className="px-10" button>
           <ListItemText style={{textDecorationLine:'none',color:'white'}} primary="PortFolio"/>
        </ListItem>
      </List>
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <nav className={classes.drawer} aria-label="mailbox folders"  style={{zIndex:0}}>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Drawer
            container={container}
            variant="permanent"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
      </nav>
    </div>
  );
}


export default SideBar;

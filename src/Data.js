import React, { Component } from "react";
import SideBar from "./SideBar";
import { Bar } from "react-chartjs-2";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { data } from "../src/Utils";

class Data extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "633",
      color: "red",
      name: "BarOne",
      flag: false,
    };
  }

  onChange = (data) => {
    this.setState({
      value: data.value,
      color: data.color,
      name: data.name,
      flag: true,
    });
  };
  render() {
    return (
      <div>
        <SideBar />
        <div style={{ margin: "30px" }}>
          <UncontrolledDropdown size="lg">
            <DropdownToggle caret>Please Select Date</DropdownToggle>
            <DropdownMenu>
              {data.map((item) => {
                return (
                  <React.Fragment>
                    <div onClick={() => this.onChange(item)}>
                      <DropdownItem>{item.date}</DropdownItem>
                      <DropdownItem divider />
                    </div>
                  </React.Fragment>
                );
              })}
            </DropdownMenu>
          </UncontrolledDropdown>
        </div>
        {this.state.flag ? (
          <div style={{ margin: "150px", paddingLeft: "250px" }}>
            <div style={{ maxWidth: "500px" }}>
              <Bar
                data={{
                  // Name of the variables on x-axies for each bar
                  labels: [this.state.name],
                  datasets: [
                    {
                      // Label for bars
                      label: "total",
                      // Data or value of your each variable
                      data: [this.state.value],
                      // Color of each bar
                      backgroundColor: [this.state.color],
                      // Border color of each bar
                      borderColor: ["black"],
                      borderWidth: 1,
                    },
                  ],
                }}
                // Height of graph
                height={500}
                options={{
                  maintainAspectRatio: false,
                  scales: {
                    yAxes: [
                      {
                        ticks: {
                          // The y-axis value will start from zero
                          beginAtZero: true,
                        },
                      },
                    ],
                  },
                  legend: {
                    labels: {
                      fontSize: 15,
                    },
                  },
                }}
              />
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default Data;
